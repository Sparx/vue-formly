import Vue from 'vue';
import VueFormly from './plugin/vue-formly';
import App from './App.vue';

Vue.config.productionTip = false;

Vue.use(VueFormly, {
  uiFramework: 'bootstrap'
});

new Vue({
  render: h => h(App),
}).$mount('#app');
