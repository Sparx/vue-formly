import Title from '@/plugin/components/Title.vue';
import FreeText from '@/plugin/components/FreeText.vue';
import Section from '@/plugin/components/Section.vue';
import Input from '@/plugin/components/Input.vue';
import Select from '@/plugin/components/Select.vue';
import Radio from '@/plugin/components/Radio.vue';
import Checkbox from '@/plugin/components/Checkbox.vue';
import SubmitButton from '@/plugin/components/SubmitButton.vue';

class ComponentFactory {
    static create(createElement, key, val) {
        switch(key.toLowerCase()) {
            case 'title':
            case 'subtitle':
            case 'sectiontitle':
                return createElement(Title, {
                    props: {
                        type: key,
                        text: val
                    }
                });
            case 'freetext':
                return createElement(FreeText, {
                    props: {
                        text: val
                    }
                });
            case 'sections':
                return val.map(v => createElement(Section, {
                    props: {
                        content: v
                    }
                }));
            case 'inputs':
                return val.map(v => this.createInput(createElement, v));
            case 'submit':
                return createElement(SubmitButton, {
                    props: {
                        ...val
                    }
                });
        }
    }

    static createInput(createElement, val) {
        switch(val.type) {
            case 'radio':
                return createElement(Radio, {
                    ref: val.name,
                    props: val
                });
            case 'select':
                return createElement(Select, {
                    ref: val.name,
                    props: val
                });
            case 'checkbox':
                return createElement(Checkbox, {
                    ref: val.name,
                    props: val
                });
            default:
                return createElement(Input, {
                    ref: val.name,
                    props: val
                });
        }
    }
}

export { ComponentFactory };
