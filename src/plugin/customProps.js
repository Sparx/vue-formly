function LabelValidator(val) {      
    if(val instanceof Object)
        return val.hasOwnProperty('text') && val.hasOwnProperty('inputId');                

    return false;
}

export {
    LabelValidator
};