const getSubmitButtonClasses = (framework = '') => {
    let classes = [ 'formly-submit' ];
    switch(framework.toLowerCase()) {
        case 'bootstrap':
            classes.push('btn');
            break;
    }
    return classes.join(' ');
};

const getInputClasses = (framework = '') => {
    let classes = [ 'formly-form-control' ];
    switch(framework.toLowerCase()) {
        case 'bootstrap':
            classes.push('form-control');
            break;
    }
    return classes.join(' ');
};

const getContainerClasses = (framework = '', width = 0) => {
    let classes = [ 'formly-form-group' ];
    switch(framework.toLowerCase()) {
        case 'bootstrap':
            classes.push('form-group');
            break;
    }

    if(framework.toLowerCase() === 'bootstrap') {
        if(!width || width < 1 || width > 12) {
            classes.push('col-12');
        } else {
            classes.push(`col-md-${width}`);
        }
    }

    return classes.join(' ');
};

const getValidationClasses = (framework = '') => {
    let classes = [ 'validation-error' ];
    switch(framework.toLowerCase()) {
        case 'bootstrap':
            classes.push('invalid-feedback');
            break;
    }

    return classes.join(' ');
}

const getSectionClasses = (framework = '') => {
    let classes = [ 'formly-form-section' ];
    switch(framework.toLowerCase()) {
        case 'bootstrap':
            classes.push('row');
            break;
    }
    return classes.join(' ');
};

const getRadioCheckContainerClasses = (framework = '', isCheckboxContainer = false) => {
    let classes = [isCheckboxContainer
        ? 'formly-form-check'
        : 'formly-form-radio'];

    switch(framework.toLowerCase()) {
        case 'bootstrap':
        classes.push('form-check');
        break;
    }

    return classes.join(' ');
};

const getRadioCheckInputClasses = (framework = '') => {
    let classes = [];
    switch(framework.toLowerCase()) {
        case 'bootstrap':
        classes.push('form-check-input');
        break;
    }

    if(classes.length) {
        return classes.join(' ');
    } else {
        return '';
    }
};

const getRadioCheckLabelClasses = (framework = '') => {
    let classes = [];
    switch(framework.toLowerCase()) {
        case 'bootstrap':
        classes.push('form-check-label');
        break;
    }

    if(classes.length) {
        return classes.join(' ');
    } else {
        return '';
    }
};

const getTitleClasses = (framework = '') => {
    let classes = [ 'formly-form-title' ];
    switch(framework.toLowerCase()) {
        case 'bootstrap':
            classes.push('col-12');
            break;
    }
    return classes.join(' ');
};

const getFreeTextClasses = (framework = '') => {
    let classes = [ 'formly-form-text' ];
    switch(framework.toLowerCase()) {
        case 'bootstrap':
            classes.push('col-12');
            break;
    }
    return classes.join(' ');
};

export { getInputClasses,
    getContainerClasses,
    getValidationClasses,
    getSectionClasses,
    getTitleClasses,
    getFreeTextClasses,
    getRadioCheckInputClasses,
    getRadioCheckLabelClasses,
    getRadioCheckContainerClasses,
    getSubmitButtonClasses };
