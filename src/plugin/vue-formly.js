import Vue from 'vue';
import VueFormlyBase from './VueFormly.vue';

const VueFormlyPlugin = {
    install(vue, opts = {}) {
        Vue.prototype.$formlyUiFramework = opts.uiFramework || '';
        Vue.prototype.$formlyEvents = new Vue();

        vue.component(VueFormlyBase.name, VueFormlyBase);
    }
};

export default VueFormlyPlugin;
