import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import sinon from 'sinon';
import axios from 'axios';
import flushPromises from 'flush-promises';
import { ADD_MODEL_PROPERTY,
    SET_MODEL_PROPERTY } from '@/plugin/constants';
import VueFormly from '@/plugin/VueFormly.vue';
import Loader from '@/plugin/components/Loader.vue';
import VueFormlyPlugin from '@/plugin/vue-formly';
import { FORM_SUBMITTING,
    FORM_SUBMITTING_COMPLETE,
    FORM_SUBMITTING_ERROR } from '@/plugin/constants';

describe('VueFormly Component', () => {
    let localVue;
    let axiosStub;

    beforeEach(() => {
        localVue = createLocalVue();
        localVue.use(VueFormlyPlugin);

        axiosStub = sinon.stub(axios, 'post');
    });

    afterEach(() => {
        axiosStub.restore();
    });

    it('it is a Vue instance', () => {
        const wrapper = shallowMount(VueFormly,
            {
                propsData: {
                    template: { }
                }
            },
            localVue);
        expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it('it renders <VueFormly> tag', () => {
        const wrapper = shallowMount(VueFormly,
            {
                propsData: {
                    template: { }
                }
            },
            localVue);
        expect(wrapper.name()).toEqual('VueFormly');
    });

    it('it renders the loader whilst template is loading', () => {
        const wrapper = shallowMount(VueFormly, {
            propsData: {
                template: { }
            }
        }, localVue);
        expect(wrapper.find(Loader).exists()).toBeTruthy();
    });

    it('it sets correct id attribute on the form', () => {
        const wrapper = shallowMount(VueFormly,
            {
                propsData: {
                    template: {
                        "title": "Test title"
                    }
                }
            },
            localVue);
        expect(wrapper.find('form').attributes().id).toEqual('vueFormlyForm');
    });

    it(`it renders template title`, () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "title": "Test title"
                    }
                }
            },
            localVue);
        expect(wrapper.find('h1').exists()).toBeTruthy();
    });

    it(`it renders template subtitle`, () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "subtitle": "Test subtitle"
                    }
                }
            },
            localVue);
        expect(wrapper.find('h2').exists()).toBeTruthy();
    });

    it(`it renders template freetext`, () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "freeText": "Test text"
                    }
                }
            },
            localVue);
        expect(wrapper.find('p').exists()).toBeTruthy();
    });

    it(`it renders template section`, () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "sections": [
                            { }
                        ]
                    }
                }
            },
            localVue);
        expect(wrapper.find('section').exists()).toBeTruthy();
    });

    it(`it renders template input`, () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "inputs": [
                            {
                                name: 'test',
                                label: 'Test label',
                            }
                        ]
                    }
                }
            },
            localVue);
        expect(wrapper.find('input').exists()).toBeTruthy();
    });

    it(`it sets ref attribute on input component`, () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "inputs": [
                            {
                                name: 'test',
                                label: 'Test label',
                            }
                        ]
                    }
                }
            },
            localVue);
        expect(wrapper.find({
            ref: 'test'
        }).exists()).toBeTruthy();
    });

    it(`it renders template radio buttons`, () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "inputs": [
                            {
                                name: "userGender",
                                label: 'Test label',
                                type: "radio",
                                options: [
                                    { value: "male", text: "Male" },
                                    { value: "female", text: "Female" }
                                ]
                            }
                        ]
                    }
                }
            },
            localVue);
        expect(wrapper.findAll('input[type=radio]').length).toEqual(2);
    });

    it(`it sets ref attribute on radio button component`, () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "inputs": [
                            {
                                name: "userGender",
                                label: 'Test label',
                                type: "radio",
                                options: [
                                    { value: "male", text: "Male" },
                                    { value: "female", text: "Female" }
                                ]
                            }
                        ]
                    }
                }
            },
            localVue);
        expect(wrapper.find({
            ref: 'userGender'
        }).exists()).toBeTruthy();
    });

    it(`it renders template checkbox buttons`, () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "inputs": [
                            {
                                name: "userGender",
                                label: 'Test label',
                                type: "checkbox",
                                options: [
                                    { value: "male", text: "Male" },
                                    { value: "female", text: "Female" }
                                ]
                            }
                        ]
                    }
                }
            },
            localVue);
        expect(wrapper.findAll('input[type=checkbox]').length).toEqual(2);
    });

    it(`it sets ref attribute on checkbox button component`, () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "inputs": [
                            {
                                name: "userGender",
                                label: 'Test label',
                                type: "checkbox",
                                options: [
                                    { value: "male", text: "Male" },
                                    { value: "female", text: "Female" }
                                ]
                            }
                        ]
                    }
                }
            },
            localVue);

        expect(wrapper.find({
            ref: 'userGender'
        }).exists()).toBeTruthy();
    });

    it('it renders template select', () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "inputs": [
                            {
                                type: 'select',
                                name: 'test',
                                options: [],
                                label: 'Test label',
                            }
                        ]
                    }
                }
            },
            localVue);
        expect(wrapper.find('select').exists()).toBeTruthy();
    });

    it('it sets the ref attribute on the select component', () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "inputs": [
                            {
                                type: 'select',
                                name: 'test',
                                options: [],
                                label: 'Test label',
                            }
                        ]
                    }
                }
            },
            localVue);
        expect(wrapper.find({
            ref: 'test'
        }).exists()).toBeTruthy();
    });

    it('it sets new model property on ADD_MODEL_PROPERTY event', () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "inputs": [
                            {
                                name: 'test',
                                label: 'Test label',
                            }
                        ]
                    }
                }
            },
            localVue);
        localVue.prototype.$formlyEvents.$emit(ADD_MODEL_PROPERTY, 'testProp');
        expect(wrapper.vm.form.testProp).toBeDefined();
    });

    it('it updates model property on SET_MODEL_PROPERTY event', () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "inputs": [
                            {
                                name: 'test',
                                label: 'Test label',
                            }
                        ]
                    }
                }
            },
            localVue);
        localVue.prototype.$formlyEvents.$emit(ADD_MODEL_PROPERTY, 'testProp');
        localVue.prototype.$formlyEvents.$emit(SET_MODEL_PROPERTY, {
            target: 'testProp',
            value: 'testValue'
        });
        expect(wrapper.vm.form.testProp).toEqual('testValue');
    });

    it('it removes the event hub listener in beforeDestroy method', () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "inputs": [
                            {
                                name: 'test',
                                label: 'Test label',
                            }
                        ]
                    }
                }
            },
            localVue);
            const stub = sinon.stub(wrapper.vm.$formlyEvents, '$off');

            wrapper.destroy();

            expect(stub.called).toBeTruthy();
            stub.restore();
    });

    it('it renders the submit button on the form', () => {
        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        "submit": { }
                    }
                }
            }, localVue);

        expect(wrapper.find('form > button[type="submit"]').exists()).toBeTruthy();
    });

    it('it calls submit method on form submit event', async () => {
        const stub = sinon.stub();

        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        submit: {
                            endpoint: '/test'
                        }
                    }
                }
            }, localVue);
        wrapper.setMethods({
            submitForm: stub
        });
        wrapper.find('form').trigger('submit');

        await flushPromises();

        expect(stub.calledOnce).toBeTruthy();
    });

    it('it calls preventDefault on form submission event', async () => {
        const stub = sinon.stub();
        const evt = {
            preventDefault: stub
        };

        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        submit: {
                            endpoint: '/submit'
                        }
                    }
                }
            }, localVue);
        wrapper.vm.submitForm(evt);

        await flushPromises();

        expect(stub.calledOnce).toBeTruthy();
    });

    it('it submits the form data with correct model info', async () => {
        const evtStub = sinon.stub();
        const evt = {
            preventDefault: evtStub
        };

        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        submit: {
                            endpoint: '/testendpoint'
                        }
                    }
                }
            }, localVue);
        wrapper.setData({
            form: {
                user: 'John Doe'
            }
        });
        wrapper.vm.submitForm(evt);

        await flushPromises();

        expect(axiosStub.calledWithExactly('/testendpoint', {
            user: 'John Doe'
        })).toBeTruthy();
    });

    it('it emits FORM_SUBMITTING event when form is submitted', async () => {
        const evtStub = sinon.stub();
        const formlyStub = sinon.stub();

        const evt = {
            preventDefault: evtStub
        };

        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        submit: {
                            endpoint: '/testendpoint'
                        }
                    }
                },
                mocks: {
                    $formlyEvents: {
                        $emit: formlyStub,
                        $on: formlyStub
                    }
                }
            }, localVue);
        wrapper.setData({
            form: {
                user: 'John Doe'
            }
        });
        wrapper.vm.submitForm(evt);

        await flushPromises();

        expect(formlyStub.calledWithExactly(FORM_SUBMITTING)).toBeTruthy();
    });

    it('it prevents further submissions when isSubmitting is true', async () => {
        const evtStub = sinon.stub();
        const formlyStub = sinon.stub();

        const evt = {
            preventDefault: evtStub
        };

        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        submit: {
                            endpoint: '/testendpoint'
                        }
                    }
                },
                mocks: {
                    $formlyEvents: {
                        $emit: formlyStub,
                        $on: formlyStub
                    }
                }
            }, localVue);
        wrapper.setData({
            isSubmitting: true
        });
        wrapper.vm.submitForm(evt);

        expect(axiosStub.called).toBeFalsy();
    });

    it('it sets isSubmitting to false when form submission is completed', async () => {
        const evtStub = sinon.stub();
        const formlyStub = sinon.stub();

        const evt = {
            preventDefault: evtStub
        };

        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        submit: {
                            endpoint: '/testendpoint'
                        }
                    }
                },
                mocks: {
                    $formlyEvents: {
                        $emit: formlyStub,
                        $on: formlyStub
                    }
                }
            }, localVue);
        wrapper.setData({
            form: {
                user: 'John Doe'
            }
        });
        wrapper.vm.submitForm(evt);

        await flushPromises();

        expect(wrapper.vm.isSubmitting).toBeFalsy();
    });

    it('it emits FORM_SUBMITTING_COMPLETE event when form is submitted successfully', async () => {
        const evtStub = sinon.stub();
        const formlyStub = sinon.stub();

        const evt = {
            preventDefault: evtStub
        };

        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        submit: {
                            endpoint: '/testendpoint'
                        }
                    }
                },
                mocks: {
                    $formlyEvents: {
                        $emit: formlyStub,
                        $on: formlyStub
                    }
                }
            }, localVue);
        wrapper.setData({
            form: {
                user: 'John Doe'
            }
        });
        wrapper.vm.submitForm(evt);

        await flushPromises();

        expect(formlyStub.calledWithExactly(FORM_SUBMITTING_COMPLETE)).toBeTruthy();
    });

    it('it emits FORM_SUBMITTING_ERROR event when form submission throws', async () => {
        const evtStub = sinon.stub();
        const formlyStub = sinon.stub();

        axiosStub.throws(new Error('Test error'));

        const evt = {
            preventDefault: evtStub
        };

        const wrapper = mount(VueFormly,
            {
                propsData: {
                    template: {
                        submit: {
                            endpoint: '/testendpoint'
                        }
                    }
                },
                mocks: {
                    $formlyEvents: {
                        $emit: formlyStub,
                        $on: formlyStub
                    }
                }
            }, localVue);
        wrapper.setData({
            form: {
                user: 'John Doe'
            }
        });
        wrapper.vm.submitForm(evt);

        await flushPromises();

        expect(formlyStub.calledWithExactly(FORM_SUBMITTING_ERROR)).toBeTruthy();
    });
});
