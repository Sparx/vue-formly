import {mount, createLocalVue} from '@vue/test-utils';
import sinon from 'sinon';
import axios from 'axios';
import flushPromises from 'flush-promises';
import VueFormly from '@/plugin/VueFormly.vue';
import VueFormlyPlugin from '@/plugin/vue-formly';
import { ADD_MODEL_PROPERTY_VALIDATION } from '@/plugin/constants';

describe('VueFormly Component - Validation', () => {
	let localVue;
	let axiosStub;

	beforeEach(() => {
		localVue = createLocalVue();
		localVue.use(VueFormlyPlugin);
		axiosStub = sinon.stub(axios, 'post');
	});

	afterEach(() => {
		axiosStub.restore();
	});

	it('it sets hasValidation to true on ADD_MODEL_PROPERTY_VALIDATION event', () => {
		const inputs = [
			{
				name: 'test',
				label: 'Test label',
				validation: {
					required: true
				}
			}
		];

		const wrapper = mount(VueFormly,
			{
				propsData: {
					template: {
						inputs
					}
				}
			},
			localVue);

		localVue.prototype.$formlyEvents.$emit(ADD_MODEL_PROPERTY_VALIDATION, {
			target: 'test',
			validation: inputs[0].validation
		});
		expect(wrapper.vm.hasValidation).toBeTruthy();
	});

	it('it calls the formIsValid on submit if hasValidation is true', async () => {
		const stub = sinon.stub();
		const evtStub = sinon.stub();
		const evt = {
			preventDefault: evtStub
		};

		const inputs = [
			{
				name: 'test',
				label: 'Test label',
				validation: {
					required: true
				}
			}
		];

		const wrapper = mount(VueFormly, {
			propsData: {
				template: {
					inputs
				}
			}
		}, localVue);

		wrapper.setMethods({
			formIsValid: stub
		});

		wrapper.vm.submitForm(evt);

		await flushPromises();

		expect(stub.calledOnce).toBeTruthy();
	});

	it('it calls the validate method on each component', async () => {
		const evtStub = sinon.stub();
		const evt = {
			preventDefault: evtStub
		};

		const inputs = [
			{
				name: 'test',
				label: 'Test label',
				validation: {
					required: true
				}
			}
		];

		// TODO: Why does vue-test-utils not stub properly?
		const wrapper = mount(VueFormly, {
			propsData: {
				template: {
					inputs
				}
			}
		}, localVue);
		const validateStub = sinon.stub(wrapper.vm.$refs['test'], 'validate');
		validateStub.returns(false);

		wrapper.vm.submitForm(evt);

		await flushPromises();

		expect(validateStub.calledOnce).toBeTruthy();

		wrapper.vm.$refs['test'].validate.restore();
	});

	it('it does not call the validateModel on submit if hasValidation is false', async () => {
		const stub = sinon.stub();
		const evtStub = sinon.stub();
		const evt = {
			preventDefault: evtStub
		};

		const inputs = [
			{
				name: 'test',
				label: 'Test label'
			}
		];

		const wrapper = mount(VueFormly, {
			propsData: {
				template: {
					inputs
				}
			}
		}, localVue);

		wrapper.setMethods({
			formIsValid: stub
		});

		wrapper.vm.submitForm(evt);

		await flushPromises();

		expect(stub.notCalled).toBeTruthy();
	});

	it('it prevents form submission if formIsValid returns false', async () => {
		const evtStub = sinon.stub();
		const evt = {
			preventDefault: evtStub
		};

		const inputs = [
			{
				name: 'test',
				label: 'Test label',
				validation: {
					required: true
				}
			}
		];

		const wrapper = mount(VueFormly, {
			propsData: {
				template: {
					inputs,
					submit:  {
						endpoint: '/fakepost'
					}
				}
			}
		}, localVue);
		const validateStub = sinon.stub(wrapper.vm.$refs['test'], 'validate');
		validateStub.returns(false);

		wrapper.vm.submitForm(evt);

		await flushPromises();

		expect(axiosStub.called).toBeFalsy();

		wrapper.vm.$refs['test'].validate.restore();
	});
});
