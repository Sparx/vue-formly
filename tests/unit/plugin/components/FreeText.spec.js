import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import VueFormly from '@/plugin/vue-formly';
import FreeText from '@/plugin/components/FreeText.vue';

describe('FreeText Component', () => {
    let localVue;

    beforeEach(() => {
        localVue = createLocalVue();
        localVue.use(VueFormly);
    });

    it('it is a Vue instance', () => {
        const wrapper = shallowMount(FreeText, {
            propsData: {
                text: 'Test'
            }
        });
        expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it('it renders <FormlyFreeText> tag', () => {
        const wrapper = shallowMount(FreeText, {
            propsData: {
                text: 'This is some test text'
            }
        });
        expect(wrapper.name()).toEqual('FormlyFreeText');
    });

    it('it renders the text', () => {
        const wrapper = mount(FreeText, {
            propsData: {
                text: 'This is some test text'
            }
        });
        const freeText = wrapper.find('p');
        expect(freeText.text()).toEqual('This is some test text');
    });

    it(`it adds 'formly-form-text' class to container`, () => {
        const wrapper = mount(FreeText, {
            propsData: {
                text: 'test',
            }
        }, localVue);
        expect(wrapper.classes()).toContain('formly-form-text');
    });

    it(`it adds 'col-12' class to container if $formlyUiFramework is set to bootstrap`, () => {
        const localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });

        const wrapper = mount(FreeText, {
            propsData: {
                text: 'test'
            }
        }, localVue);
        expect(wrapper.classes()).toContain('col-12');
    });
});
