import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import sinon from 'sinon';
import VueFormly from '@/plugin/vue-formly';
import Input from '@/plugin/components/Input.vue';
import { ADD_MODEL_PROPERTY,
    SET_MODEL_PROPERTY,
    FORM_SUBMITTING,
    FORM_SUBMITTING_COMPLETE,
    FORM_SUBMITTING_ERROR } from '@/plugin/constants';

describe('Input Component', () => {
    let localVue;

    beforeEach(() => {
        localVue = createLocalVue();
        localVue.use(VueFormly);
    });

    it('it is a Vue instance', () => {
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        });
        expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it(`it renders container <FormlyInput> tag`, () => {
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        });
        expect(wrapper.name()).toEqual('FormlyInput');
    });

    it(`it renders <input> tag`, () => {
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        });
        expect(wrapper.find('input').exists()).toBeTruthy();
    });

    it(`it sets default type of text if prop isn't passed`, () => {
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        });
        expect(wrapper.find('input').attributes().type).toEqual('text');
    });

    it(`it sets the type attribute`, () => {
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'userEmail',
                type: 'email',
                label: 'Test Label'
            }
        });
        expect(wrapper.find('input').attributes().type).toEqual('email');
    });

    it(`it sets the placeholder attribute`, () => {
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'userEmail',
                type: 'email',
                placeholder: 'user@domain.com',
                label: 'Test Label'
            }
        });
        expect(wrapper.find('input').attributes().placeholder).toEqual('user@domain.com');
    });

    it(`it sets the id attribute`, () => {
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'userEmail',
                label: 'Test Label'
            }
        });
        expect(wrapper.find('input').attributes().id).toEqual('userEmail');
    });

    it(`it sets the name attribute`, () => {
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'userEmail',
                label: 'Test Label'
            }
        });
        expect(wrapper.find('input').attributes().name).toEqual('userEmail');
    });

    it(`it renders the <label> tag`, () => {
        const wrapper = mount(Input, {
            localVue,
            propsData: {
                name: 'userEmail',
                label: 'Test Label'
            }
        });
        expect(wrapper.find('label').exists()).toBeTruthy();
    });

    it(`it adds 'formly-form-group' class to container`, () => {
        let localVue = createLocalVue();
        localVue.use(VueFormly);
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        });

        expect(wrapper.classes()).toContain('formly-form-group');
    });

    it(`it adds 'formly-form-control' class to input`, () => {
        let localVue = createLocalVue();
        localVue.use(VueFormly);
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        });

        const input = wrapper.find('input');
        expect(input.classes()).toContain('formly-form-control');
    });

    it(`it adds 'form-control' class to input if $formlyUiFramework is set to bootstrap`, () => {
        let localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        });

        const input = wrapper.find('input');
        expect(input.classes()).toContain('form-control');
    });

    it(`it adds 'form-group' class to container if $formlyUiFramework is set to bootstrap`, () => {
        let localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        });

        expect(wrapper.classes()).toContain('form-group');
    });

    it(`it adds 'col-12' class to container if $formlyUiFramework is set to bootstrap and no width is set`, () => {
        let localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        });

        expect(wrapper.classes()).toContain('col-md-12');
    });

    it(`it adds 'col-x' class to container if $formlyUiFramework is set to bootstrap where x > 0 and x <= 12`, () => {
        let localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });
        const wrapper = shallowMount(Input, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label',
                width: 6
            }
        });

        expect(wrapper.classes()).toContain('col-md-6');
    });

    it(`it emits ADD_MODEL_PROPERTY when adding an element with correct name`, () => {
        const stub = sinon.stub();

        mount(Input, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label'
            },
            mocks: {
                $formlyEvents: {
                    $emit: stub,
                    $on: stub,
                    $off: stub
                }
            }
        });

        expect(stub.calledWithExactly(ADD_MODEL_PROPERTY, 'test')).toBeTruthy();
    });

    it(`it emits SET_MODEL_PROPERTY event when value changes with correct payload`, () => {
        const stub = sinon.stub();

        const wrapper = mount(Input, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label'
            },
            mocks: {
                $formlyEvents: {
                    $emit: stub,
                    $on: stub,
                    $off: stub
                }
            }
        });

        const input = wrapper.find('input');
        input.element.value = 'value';
        input.trigger('input');

        expect(stub.calledWithExactly(SET_MODEL_PROPERTY, {
            target: 'test',
            value: 'value'
        })).toBeTruthy();
    });

    // it(`it renders a textarea if type is multiline`, () => {
    //     const wrapper = shallowMount(Input, {
    //         localVue,
    //         propsData: {
    //             name: 'userDetails',
    //             type: 'multiline',
    //             label: 'Test Label'
    //         }
    //     });
    //     expect(wrapper.find('textarea').exists()).toBeTruthy();
    // });
    //
    // it(`it does not add multiline type to textarea HTML element`, () => {
    //     const wrapper = shallowMount(Input, {
    //         localVue,
    //         propsData: {
    //             name: 'userDetails',
    //             type: 'multiline',
    //             label: 'Test Label'
    //         }
    //     });
    //     expect(wrapper.find('textarea').attributes().type).toBeUndefined();
    // });

    it('it adds the aria-labelledby attribute on the input element', () => {
        const wrapper = mount(Input, {
            localVue,
            propsData: {
                name: 'userDetails',
                label: 'Test Label'
            }
        });
        expect(wrapper.find('input').attributes()['aria-labelledby']).toEqual('userDetailsLabel');
    });

    it('it sets isSubmitting to true on FORM_SUBMITTING event', () => {
        const wrapper = mount(Input, {
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        }, localVue);
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING);

        expect(wrapper.vm.isSubmitting).toBeTruthy();
    });

    it('it disables the input on FORM_SUBMITTING event', () => {
        const wrapper = mount(Input, {
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        }, localVue);
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING);

        const input = wrapper.find('input');
        expect(input.attributes('disabled')).toBeTruthy();
    });

    // it('it disables the textarea on FORM_SUBMITTING event', () => {
    //     const wrapper = mount(Input, {
    //         propsData: {
    //             name: 'test',
    //             label: 'Test Label',
    //             type: 'multiline'
    //         }
    //     }, localVue);
    //     localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING);
    //
    //     const input = wrapper.find('textarea');
    //     expect(input.attributes('disabled')).toBeTruthy();
    // });

    it('it sets isSubmitting to false on FORM_SUBMITTING_COMPLETE event', () => {
        const wrapper = mount(Input, {
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        }, localVue);
        wrapper.setData({
            isSubmitting: true
        });
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_COMPLETE);

        expect(wrapper.vm.isSubmitting).toBeFalsy();
    });

    it('it enables the input when on FORM_SUBMITTING_COMPLETE event', () => {
        const wrapper = mount(Input, {
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        }, localVue);
        wrapper.setData({
            isSubmitting: true
        });
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_COMPLETE);

        const input = wrapper.find('input');
        expect(input.attributes('disabled')).toBeFalsy();
    });

    // it('it enables the textarea when on FORM_SUBMITTING_COMPLETE event', () => {
    //     const wrapper = mount(Input, {
    //         propsData: {
    //             name: 'test',
    //             label: 'Test Label',
    //             type: 'multiline'
    //         }
    //     }, localVue);
    //     wrapper.setData({
    //         isSubmitting: true
    //     });
    //     localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_COMPLETE);
    //
    //     const input = wrapper.find('textarea');
    //     expect(input.attributes('disabled')).toBeFalsy();
    // });

    it('it sets isSubmitting to false on FORM_SUBMITTING_ERROR event', () => {
        const wrapper = mount(Input, {
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        }, localVue);
        wrapper.setData({
            isSubmitting: true
        });
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_ERROR);
        expect(wrapper.vm.isSubmitting).toBeFalsy();
    });

    it('it enables the input when on FORM_SUBMITTING_ERROR event', () => {
        const wrapper = mount(Input, {
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        }, localVue);
        wrapper.setData({
            isSubmitting: true
        });
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_ERROR);

        const input = wrapper.find('input');
        expect(input.attributes('disabled')).toBeFalsy();
    });

    // it('it enables the textarea when on FORM_SUBMITTING_ERROR event', () => {
    //     const wrapper = mount(Input, {
    //         propsData: {
    //             name: 'test',
    //             label: 'Test Label',
    //             type: 'multiline'
    //         }
    //     }, localVue);
    //     wrapper.setData({
    //         isSubmitting: true
    //     });
    //     localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_ERROR);
    //
    //     const input = wrapper.find('textarea');
    //     expect(input.attributes('disabled')).toBeFalsy();
    // });

    it('it removes the event hub listener in beforeDestroy method', () => {
        const wrapper = mount(Input, {
            propsData: {
                name: 'test',
                label: 'Test Label'
            }
        }, localVue);
        const stub = sinon.stub(wrapper.vm.$formlyEvents, '$off');

        wrapper.destroy();

        expect(stub.called).toBeTruthy();
        stub.restore();
    });
});
