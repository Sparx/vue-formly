import {mount, createLocalVue, shallowMount} from '@vue/test-utils';
import sinon from 'sinon';
import VueFormly from '@/plugin/vue-formly';
import Input from '@/plugin/components/Input.vue';
import { ADD_MODEL_PROPERTY_VALIDATION } from '@/plugin/constants';

describe('Input Component - Validation', () => {
	let localVue;

	beforeEach(() => {
		localVue = createLocalVue();
		localVue.use(VueFormly);
	});

	it('it emits the ADD_MODEL_PROPERTY_VALIDATION event if the element validation', () => {
		const stub = sinon.stub();

		mount(Input, {
			localVue,
			propsData: {
				name: 'test',
				label: 'Test Label',
				validation: {
					required: true
				}
			},
			mocks: {
				$formlyEvents: {
					$emit: stub,
					$on: stub,
					$off: stub
				}
			}
		});

		expect(stub.calledWith(ADD_MODEL_PROPERTY_VALIDATION)).toBeTruthy();
	});

	it('it emits the ADD_MODEL_PROPERTY_VALIDATION event with correct payload', () => {
		const stub = sinon.stub();
		const props = {
			name: 'test',
			label: 'Test Label',
			validation: {
				required: true
			}
		};

		mount(Input, {
			localVue,
			propsData: props,
			mocks: {
				$formlyEvents: {
					$emit: stub,
					$on: stub,
					$off: stub
				}
			}
		});

		expect(stub.calledWithExactly(ADD_MODEL_PROPERTY_VALIDATION, {
			target: 'test',
			validation: props.validation
		})).toBeTruthy();
	});

	it('it renders validation error <span/> if element has validation', () =>{
		const stub = sinon.stub();
		const props = {
			name: 'test',
			label: 'Test Label',
			validation: {
				required: true
			}
		};

		const wrapper = mount(Input, {
			localVue,
			propsData: props,
			mocks: {
				$formlyEvents: {
					$emit: stub,
					$on: stub,
					$off: stub
				}
			}
		});

		expect(wrapper.find('span.validation-error').exists()).toBeTruthy();
	});

	it('it updates validation error with correct error message', () =>{
		const stub = sinon.stub();
		const props = {
			name: 'test',
			label: 'Test Label',
			validation: {
				required: true
			}
		};

		const wrapper = mount(Input, {
			localVue,
			propsData: props,
			mocks: {
				$formlyEvents: {
					$emit: stub,
					$on: stub,
					$off: stub
				}
			}
		});

		wrapper.vm.updateValidationError('Test error');

		expect(wrapper.find('span.validation-error').text()).toEqual('Test error');
	});

	it(`it adds 'invalid-feedback' class to validation span if $formlyUiFramework is set to bootstrap`, () => {
		let localVue = createLocalVue();
		localVue.use(VueFormly, {
			uiFramework: 'bootstrap'
		});

		const props = {
			name: 'test',
			label: 'Test Label',
			validation: {
				required: true
			}
		};

		const wrapper = shallowMount(Input, {
			localVue,
			propsData: props
		});

		const span = wrapper.find('span.validation-error');
		expect(span.classes()).toContain('invalid-feedback');
	});
});
