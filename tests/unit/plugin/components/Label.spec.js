import { mount } from '@vue/test-utils';
import Label from '@/plugin/components/Label.vue';

describe('Label Component', () => {
    it('it is a Vue instance', () => {
        const wrapper = mount(Label, {
            propsData: {
                text: 'test',
                inputId: 'testInput'
            }
        });
        expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it(`it renders <FormlyLabel> tag`, () => {
        const wrapper = mount(Label, {
            propsData: {
                text: 'test',
                inputId: 'testInput'
            }
        });
        expect(wrapper.name()).toEqual('FormlyLabel');
    });

    it('it sets the id attribute', () => {
        const wrapper = mount(Label, {
            propsData: {
                text: 'test',
                inputId: 'testInput'
            }
        });
        expect(wrapper.attributes().id).toEqual('testInputLabel');
    });

    it('it sets the for attribute', () => {
        const wrapper = mount(Label, {
            propsData: {
                text: 'test',
                inputId: 'testInput'
            }
        });
        expect(wrapper.attributes().for).toEqual('testInput');
    });

    it('it sets the label content', () => {
        const wrapper = mount(Label, {
            propsData: {
                text: 'test',
                inputId: 'testInput'
            }
        });
        expect(wrapper.text()).toEqual('test');
    });
});
