import { mount } from '@vue/test-utils';
import Loader from '@/plugin/components/Loader.vue';

describe('Loader Component', () => {
    it('it is a Vue instance', () => {
        const wrapper = mount(Loader);
        expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it('it adds formly-loader class to container', () => {
        const wrapper = mount(Loader);
        expect(wrapper.classes()).toContain('formly-loader');
    });

    it('it renders the loading animation container', () => {
        const wrapper = mount(Loader);
        expect(wrapper.find('div.lds-ripple').exists()).toBeTruthy();
    });

    it('it renders the loading message', () => {
        const wrapper = mount(Loader);
        expect(wrapper.find('p').text()).toEqual('Preparing form...');
    });
});