import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import sinon from 'sinon';
import VueFormly from '@/plugin/vue-formly';
import Radio from '@/plugin/components/Radio.vue';
import { ADD_MODEL_PROPERTY, 
    SET_MODEL_PROPERTY,
    FORM_SUBMITTING,
    FORM_SUBMITTING_COMPLETE,
    FORM_SUBMITTING_ERROR } from '@/plugin/constants';

describe('Radio Component', () => {
    let localVue;

    beforeEach(() => {
        localVue = createLocalVue();
        localVue.use(VueFormly);
    });

    it('it is a Vue instance', () => {
        const wrapper = shallowMount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test label',
                options: []
            }
        });
        expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it(`it renders container <FormlyRadio> tag`, () => {
        const wrapper = shallowMount(Radio,{
            propsData: {
                name: 'test',
                label: 'Test label',
                options: []
            }
        });
        expect(wrapper.name()).toEqual('FormlyRadio');
    });

    it(`it adds 'formly-form-group' class to container`, () => {
        const wrapper = shallowMount(Radio,{
            propsData: {
                name: 'test',
                label: 'Test label',
                options: []
            }
        });

        expect(wrapper.classes()).toContain('formly-form-group');
    });

    it(`it adds 'formly-form-radio-group' class to container`, () => {
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test label',
                options: []
            }
        });

        expect(wrapper.classes()).toContain('formly-form-radio-group');
    });

    it(`it adds 'col-12' class to container if $formlyUiFramework is set to bootstrap and no width is set`, () => {
        const localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });

        const wrapper = mount(Radio, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test label',
                options: []
            }
        });

        expect(wrapper.classes()).toContain('col-md-12');
    });

    it(`it adds 'col-x' class to container if $formlyUiFramework is set to bootstrap where x > 0 and x <= 12`, () => {
        const localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });

        const wrapper = mount(Radio, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [],
                width: 6
            }
        });

        expect(wrapper.classes()).toContain('col-md-6');
    });

    it(`it renders control container with 'formly-form-radio' class`, () => {
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'male', text: 'male' }
                ]
            }
        });

        const controlWrapper = wrapper.find('div.formly-form-radio');
        expect(controlWrapper.exists()).toBeTruthy();
    });

    it(`it renders control label`, () => {
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'male', text: 'male' }
                ]
            }
        });

        const controlLabel = wrapper.find('div.formly-form-radio-group > fieldset > p');
        expect(controlLabel.text()).toEqual('Test label');
    });

    it(`it renders radio label with indexed 'for' attribute`, () => {
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        });

        const radioLabel = wrapper.find('div.formly-form-radio:first-of-type > label');
        expect(radioLabel.attributes('for')).toEqual('test0');
    });

    it(`it renders radio label with provided text`, () => {
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        });

        const radioLabel = wrapper.find('div.formly-form-radio:first-of-type > label');
        expect(radioLabel.text()).toEqual('Male');
    });

    it(`it renders radio label with value as text if no text is passed on option`, () => {
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'male' }
                ]
            }
        });

        const radioLabel = wrapper.find('div.formly-form-radio:first-of-type > label');
        expect(radioLabel.text()).toEqual('male');
    });

    it(`it renders input with type of 'radio'`, () => {
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        });

        const radio = wrapper.find('div.formly-form-radio:first-of-type > input[type=radio]');
        expect(radio.exists()).toBeTruthy();
    });

    it(`it renders radio input with correct value attribute`, () => {
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        });

        const radio = wrapper.find('div.formly-form-radio:first-of-type > input[type=radio]');
        expect(radio.attributes('value')).toEqual('male');
    });

    it(`it renders radio input with indexed 'id' attribute`, () => {
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        });

        const radio = wrapper.find('div.formly-form-radio:first-of-type > input[type=radio]');
        expect(radio.attributes('id')).toEqual('test0');
    });

    it(`it renders radio input with correct name attribute`, () => {
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        });

        const radio = wrapper.find('div.formly-form-radio:first-of-type > input[type=radio]');
        expect(radio.attributes('name')).toEqual('test');
    });

    it(`it emits ADD_MODEL_PROPERTY when adding element with correct name`, () => {
        const stub = sinon.stub();

        mount(Radio, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'value text' }
                ]
            },
            mocks: {
                $formlyEvents: {
                    $emit: stub,
                    $on: stub,
                    $off: stub
                }
            }
        });

        expect(stub.calledWithExactly(ADD_MODEL_PROPERTY, 'test')).toBeTruthy();
    });

    it(`it emits SET_MODEL_PROPERTY event when element is checked with correct payload`, () => {
        const stub = sinon.stub();

        const wrapper = mount(Radio, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'value text' }
                ]
            },
            mocks: {
                $formlyEvents: {
                    $emit: stub,
                    $on: stub,
                    $off: stub
                }
            }
        });

        const radio = wrapper.find('input#test0');        
        radio.trigger('change');

        expect(stub.calledWithExactly(SET_MODEL_PROPERTY, {
            target: 'test',
            value: 'value text'
        })).toBeTruthy();
    });

    it(`it renders control container with 'form-check' class if $formlyUiFramework is bootstrap`, () => {
        const localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });

        const wrapper = mount(Radio, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'male', text: 'male' }
                ]
            }
        });

        const controlWrapper = wrapper.find('div.formly-form-radio');
        expect(controlWrapper.classes()).toContain('form-check');
    });

    it(`it renders radio label with 'form-check-label' class if $formlyUiFramework is bootstrap`, () => {
        const localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });

        const wrapper = mount(Radio, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        });

        const radioLabel = wrapper.find('div.formly-form-radio:first-of-type > label');
        expect(radioLabel.classes()).toContain('form-check-label');
    });

    it(`it renders radio input with class of 'form-check-input' if $formlyUiFramework is bootrstrap`, () => {
        const localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });

        const wrapper = mount(Radio, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        });

        const radio = wrapper.find('div.formly-form-radio:first-of-type > input[type=radio]');
        expect(radio.classes()).toContain('form-check-input');
    });

    it(`it renders input with correct aria-labelledby attribute`, () => {
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        });

        const radio = wrapper.find('div.formly-form-radio:first-of-type > input[type=radio]');
        expect(radio.attributes()['aria-labelledby']).toEqual('test0Label');
    });

    it('it sets isSubmitting to true on FORM_SUBMITTING event', () => {      
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING);
        
        expect(wrapper.vm.isSubmitting).toBeTruthy();
    });

    it('it disables the fieldset on FORM_SUBMITTING event', () => {                   
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING);

        const fieldset = wrapper.find('fieldset');
        expect(fieldset.attributes('disabled')).toBeTruthy();
    });

    it('it sets isSubmitting to false on FORM_SUBMITTING_COMPLETE event', () => {        
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        wrapper.setData({
            isSubmitting: true
        });
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_COMPLETE);
        
        expect(wrapper.vm.isSubmitting).toBeFalsy();
    });

    it('it enables the fieldset when on FORM_SUBMITTING_COMPLETE event', () => {        
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        wrapper.setData({
            isSubmitting: true
        });
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_COMPLETE);

        const fieldset = wrapper.find('fieldset');
        expect(fieldset.attributes('disabled')).toBeFalsy();
    });

    it('it sets isSubmitting to false on FORM_SUBMITTING_ERROR event', () => {        
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        wrapper.setData({
            isSubmitting: true
        });
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_ERROR);        
        expect(wrapper.vm.isSubmitting).toBeFalsy();
    });

    it('it enables the fieldset when on FORM_SUBMITTING_ERROR event', () => {        
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        wrapper.setData({
            isSubmitting: true
        });
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_ERROR);

        const fieldset = wrapper.find('fieldset');
        expect(fieldset.attributes('disabled')).toBeFalsy();
    });

    it('it removes the event hub listener in beforeDestroy method', () => {
        const wrapper = mount(Radio, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        const stub = sinon.stub(wrapper.vm.$formlyEvents, '$off');

        wrapper.destroy();

        expect(stub.called).toBeTruthy();
        stub.restore();
    });
});
