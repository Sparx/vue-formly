import { shallowMount, createLocalVue } from '@vue/test-utils';
import VueFormly from '@/plugin/vue-formly';
import sinon from 'sinon';
import { ComponentFactory } from '@/plugin/componentFactory';
import Section from '@/plugin/components/Section.vue';

describe('Section Component', () => {
    it('it is a Vue instance', () => {
        const wrapper = shallowMount(Section, {
            propsData: {
                content: { }
            }
        });
        expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it('it renders <FormlySection> tag', () => {
        const wrapper = shallowMount(Section, {
            propsData: {
                content: { }
            }
        });
        expect(wrapper.name()).toEqual('FormlySection');
    });

    it('it calls componentFactory to render section content', () => {
        const spy = sinon.spy(ComponentFactory, 'create');

        const content = {
            'title': 'Section title'
        };

        shallowMount(Section, {
            propsData: {
                content
            }
        });

        expect(spy.calledOnce).toBeTruthy();
        ComponentFactory.create.restore();
    });

    it('it calls componentFactory with correct arguments', () => {
        const spy = sinon.spy(ComponentFactory, 'create');

        const content = {
            'title': 'Section title'
        };

        shallowMount(Section, {
            propsData: {
                content
            }
        });

        expect(spy.withArgs(content)).toBeTruthy();
        ComponentFactory.create.restore();
    });

    it(`it adds 'formly-form-section' class to section`, () => {
        let localVue = createLocalVue();
        localVue.use(VueFormly);

        const content = {
            'title': 'Section title'
        };

        const wrapper = shallowMount(Section, {
            localVue,
            propsData: {
                content
            }
        });

        expect(wrapper.classes()).toContain('formly-form-section');
    });

    it(`it adds 'row' class to section if $formlyUiFramework is set to bootstrap`, () => {
        let localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });

        const content = {
            'title': 'Section title'
        };

        const wrapper = shallowMount(Section, {
            localVue,
            propsData: {
                content
            }
        });

        expect(wrapper.classes()).toContain('row');
    });
});
