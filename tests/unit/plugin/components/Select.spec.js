import { mount, createLocalVue } from '@vue/test-utils';
import sinon from 'sinon';
import VueFormly from '@/plugin/vue-formly';
import Select from '@/plugin/components/Select.vue';
import { ADD_MODEL_PROPERTY, 
    SET_MODEL_PROPERTY,
    FORM_SUBMITTING,
    FORM_SUBMITTING_COMPLETE,
    FORM_SUBMITTING_ERROR } from '@/plugin/constants';

describe('Select Component', () => {
    let localVue;

    beforeEach(() => {
        localVue = createLocalVue();
        localVue.use(VueFormly);
    });

    it('it is a Vue instance', () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: []
            }
        });
        expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it(`it renders container <FormlySelect> tag`, () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: []
            }
        });
        expect(wrapper.name()).toEqual('FormlySelect');
    });

    it('it renders <select> tag', () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: []
            }
        });
        expect(wrapper.find('select').exists()).toBeTruthy();
    });

    it('it sets the id attribute on the select element', () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: []
            }
        });
        expect(wrapper.find('select').attributes().id).toEqual('test');
    });

    it('it renders <label> tag', () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: []
            }
        });
        expect(wrapper.find('label').exists()).toBeTruthy();
    });

    it('it sets default option text if none is passed on props', () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: []
            }
        });
        const opt = wrapper.find('select > option:first-of-type');
        expect(opt.exists()).toBeTruthy();
    });

    it('it selects default option', () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: []
            }
        });
        const opt = wrapper.find('select > option:first-of-type');
        expect(opt.attributes().selected).toBeTruthy();
    });

    it('it sets default option to disabled', () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: []
            }
        });
        const opt = wrapper.find('select > option:first-of-type');
        expect(opt.attributes().disabled).toBeTruthy();
    });

    it('it sets custom default option text', () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                placeholder: 'Pick one bitch!',
                options: []
            }
        });
        const opt = wrapper.find('select > option:first-of-type');
        expect(opt.text()).toEqual('Pick one bitch!');
    });

    it('it renders <option> tags', () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                placeholder: 'Pick one bitch!',
                options: [
                    { value: '1', text: '1' },
                    { value: '2', text: '2' },
                    { value: '3', text: '3' }
                ]
            }
        });
        const opts = wrapper.findAll('select > option:not(:first-of-type)');
        expect(opts.length).toEqual(3);
    });

    it('it renders <option> value as text if no text present', () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                placeholder: 'Pick one bitch!',
                options: [
                    { value: 'value text' }
                ]
            }
        });

        const opts = wrapper.findAll('select > option');
        expect(opts.at(1).text()).toEqual('value text');
    });

    it(`it adds 'formly-form-group' class to container`, () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                placeholder: 'Pick one bitch!',
                options: [
                    { value: 'value text' }
                ]
            }
        });

        expect(wrapper.classes()).toContain('formly-form-group');
    });

    it(`it adds 'formly-form-control' class to select`, () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                placeholder: 'Pick one bitch!',
                options: [
                    { value: 'value text' }
                ]
            }
        });

        const input = wrapper.find('select');
        expect(input.classes()).toContain('formly-form-control');
    });

    it(`it adds 'form-control' class to select if $formlyUiFramework is set to bootstrap`, () => {
        let localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                placeholder: 'Pick one bitch!',
                options: [
                    { value: 'value text' }
                ]
            }
        });

        const input = wrapper.find('select');
        expect(input.classes()).toContain('form-control');
    });

    it(`it adds 'form-group' class to container if $formlyUiFramework is set to bootstrap`, () => {
        let localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                placeholder: 'Pick one bitch!',
                options: [
                    { value: 'value text' }
                ]
            }
        });

        expect(wrapper.classes()).toContain('form-group');
    });

    it(`it emits ADD_MODEL_PROPERTY once component has been mounted with correct name`, () => {
        const stub = sinon.stub();

        mount(Select, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label',
                placeholder: 'Pick one bitch!',
                options: [
                    { value: 'value text' }
                ]
            },
            mocks: {
                $formlyEvents: {
                    $emit: stub,
                    $on: stub,
                    $off: stub
                }
            }
        });        
        expect(stub.calledWithExactly(ADD_MODEL_PROPERTY, 'test')).toBeTruthy();
    });

    it(`it emits SET_MODEL_PROPERTY event when element changes with correct payload`, () => {
        const stub = sinon.stub();

        const wrapper = mount(Select, {
            localVue,
            propsData: {
                name: 'test',
                label: 'Test Label',
                placeholder: 'Pick one bitch!',
                options: [
                    { value: 'value text' }
                ]
            },
            mocks: {
                $formlyEvents: {
                    $emit: stub,
                    $on: stub,
                    $off: stub
                }
            }
        });

        wrapper.setData({
            selectedValue: 'value text'
        });
        const select = wrapper.find('select');        
        select.trigger('change');

        expect(stub.calledWithExactly(SET_MODEL_PROPERTY, {
            target: 'test',
            value: 'value text'
        })).toBeTruthy();
    });

    it(`it adds 'col-12' class to container if $formlyUiFramework is set to bootstrap and no width is set`, () => {
        let localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                placeholder: 'Pick one bitch!',
                options: [
                    { value: 'value text' }
                ]
            }
        });

        expect(wrapper.classes()).toContain('col-md-12');
    });

    it(`it adds 'col-x' class to container if $formlyUiFramework is set to bootstrap where x > 0 and x <= 12`, () => {
        let localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                placeholder: 'Pick one bitch!',
                options: [
                    { value: 'value text' }
                ],
                width: 6
            }
        });

        expect(wrapper.classes()).toContain('col-md-6');
    });

    it('it sets the aria-labelledby attribute on the select element', () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: []
            }
        });
        expect(wrapper.find('select').attributes()['aria-labelledby']).toEqual('testLabel');
    });

    it('it sets isSubmitting to true on FORM_SUBMITTING event', () => {      
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING);
        
        expect(wrapper.vm.isSubmitting).toBeTruthy();
    });

    it('it disables the select on FORM_SUBMITTING event', () => {                   
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING);

        const select = wrapper.find('select');
        expect(select.attributes('disabled')).toBeTruthy();
    });

    it('it sets isSubmitting to false on FORM_SUBMITTING_COMPLETE event', () => {        
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        wrapper.setData({
            isSubmitting: true
        });
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_COMPLETE);
        
        expect(wrapper.vm.isSubmitting).toBeFalsy();
    });

    it('it enables the select when on FORM_SUBMITTING_COMPLETE event', () => {        
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        wrapper.setData({
            isSubmitting: true
        });
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_COMPLETE);

        const select = wrapper.find('select');
        expect(select.attributes('disabled')).toBeFalsy();
    });

    it('it sets isSubmitting to false on FORM_SUBMITTING_ERROR event', () => {        
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        wrapper.setData({
            isSubmitting: true
        });
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_ERROR);        
        expect(wrapper.vm.isSubmitting).toBeFalsy();
    });

    it('it enables the select when on FORM_SUBMITTING_ERROR event', () => {        
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        wrapper.setData({
            isSubmitting: true
        });
        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_ERROR);

        const select = wrapper.find('select');
        expect(select.attributes('disabled')).toBeFalsy();
    });

    it('it removes the event hub listener in beforeDestroy method', () => {
        const wrapper = mount(Select, {
            propsData: {
                name: 'test',
                label: 'Test Label',
                options: [
                    { value: 'male', text: 'Male' }
                ]
            }
        }, localVue);
        const stub = sinon.stub(wrapper.vm.$formlyEvents, '$off');

        wrapper.destroy();

        expect(stub.called).toBeTruthy();
        stub.restore();
    });
});
