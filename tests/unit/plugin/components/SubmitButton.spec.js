import { mount, createLocalVue } from '@vue/test-utils';
import sinon from 'sinon';
import VueFormly from '@/plugin/vue-formly';
import SubmitButton from '@/plugin/components/SubmitButton.vue';
import { FORM_SUBMITTING,
    FORM_SUBMITTING_COMPLETE,
    FORM_SUBMITTING_ERROR } from '@/plugin/constants';

describe('SubmitButton Component', () => {
    let localVue;
    beforeEach(() => {
        localVue = createLocalVue();
        localVue.use(VueFormly);
    });

    it('it is a Vue instance', () => {
        const wrapper = mount(SubmitButton);

        expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it('it sets isSubmitting to false on initial render', () => {
        const wrapper = mount(SubmitButton, localVue);        
        expect(wrapper.vm.isSubmitting).toBeFalsy();
    });

    it('it adds formly-submit class to button', () => {
        const wrapper = mount(SubmitButton, localVue);        
        expect(wrapper.classes()).toContain('formly-submit');
    });

    it('it adds btn class to button if $formlyUiFramework is bootstrap', () => {
        const localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });

        const wrapper = mount(SubmitButton, localVue);        
        expect(wrapper.classes()).toContain('btn');
    });   

    it('it sets default text on the button', () => {      
        const wrapper = mount(SubmitButton, localVue);        
        expect(wrapper.text()).toEqual('Submit');
    }); 

    it('it sets custom text on the button', () => {
        const wrapper = mount(SubmitButton, {
            propsData: {
                text: 'Custom Submit'
            }
        }, localVue);        
        expect(wrapper.text()).toEqual('Custom Submit');
    }); 

    it('it sets default color on the button', () => {
        const wrapper = mount(SubmitButton, localVue);                
        expect(wrapper.element.style.backgroundColor).toEqual('rgb(251, 140, 0)');
    }); 

    it('it sets custom color on the button', () => {
        const wrapper = mount(SubmitButton, {
            propsData: {
                color: 'hotpink'
            }
        }, localVue);                
        expect(wrapper.element.style.backgroundColor).toEqual('hotpink');
    }); 

    it('it sets default font color on the button', () => {
        const wrapper = mount(SubmitButton, localVue);                
        expect(wrapper.element.style.color).toEqual('white');
    }); 

    it('it sets custom font color on the button', () => {
        const wrapper = mount(SubmitButton, {
            propsData: {
                fontColor: 'hotpink'
            }
        }, localVue);                
        expect(wrapper.element.style.color).toEqual('hotpink');
    }); 

    it('it sets isSubmitting to true on the FORM_SUBMITTING event', () => {
        const wrapper = mount(SubmitButton, localVue);      

        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING);
        expect(wrapper.vm.isSubmitting).toBeTruthy();
    }); 

    it('it sets the disabled attribute on the FORM_SUBMITTING event', () => {
        const wrapper = mount(SubmitButton, localVue);      

        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING);
        expect(wrapper.attributes().disabled).toBeTruthy();
    }); 

    it('it sets isSubmitting to false on the FORM_SUBMITTING_COMPLETE event', () => {
        const wrapper = mount(SubmitButton, localVue);     
        wrapper.setData({
            isSubmitting: true
        }); 

        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_COMPLETE);
        expect(wrapper.vm.isSubmitting).toBeFalsy();
    });

    it('it removes the disabled attribute on the FORM_SUBMITTING_COMPLETE', () => {
        const wrapper = mount(SubmitButton, localVue);
        wrapper.setData({
            isSubmitting: true
        });

        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_COMPLETE);
        expect(wrapper.attributes().disabled).toBeFalsy();
    });

    it('it sets isSubmitting to false on the FORM_SUBMITTING_ERROR event', () => {
        const wrapper = mount(SubmitButton, localVue);     
        wrapper.setData({
            isSubmitting: true
        }); 

        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_ERROR);
        expect(wrapper.vm.isSubmitting).toBeFalsy();
    });

    it('it removes the disabled attribute on the FORM_SUBMITTING_ERROR', () => {
        const wrapper = mount(SubmitButton, localVue);
        wrapper.setData({
            isSubmitting: true
        });

        localVue.prototype.$formlyEvents.$emit(FORM_SUBMITTING_ERROR);
        expect(wrapper.attributes().disabled).toBeFalsy();
    });

    it('it removes the event hub listener in beforeDestroy method', () => {
        const wrapper = mount(SubmitButton, localVue);
        const stub = sinon.stub(wrapper.vm.$formlyEvents, '$off');

        wrapper.destroy();

        expect(stub.called).toBeTruthy();
        stub.restore();
    });
});