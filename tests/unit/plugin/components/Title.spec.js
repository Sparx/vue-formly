import { mount, createLocalVue } from '@vue/test-utils';
import VueFormly from '@/plugin/vue-formly';
import Title from '@/plugin/components/Title.vue';

describe('Title Component', () => {
    let localVue;

    beforeEach(() => {
        localVue = createLocalVue();
        localVue.use(VueFormly);
    });

    it('it is a Vue instance', () => {
        const wrapper = mount(Title, {
            propsData: {
                type: 'title',
                text: 'Test Title'
            }
        });
        expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it(`it renders <h1> tag for 'title' type`, () => {
        const wrapper = mount(Title, {
            propsData: {
                type: 'title',
                text: 'Test Title'
            }
        });
        expect(wrapper.find('h1').exists()).toBeTruthy();
    });

    it('it sets correct text for title', () => {
        const wrapper = mount(Title, {
            propsData: {
                type: 'title',
                text: 'Test Title'
            }
        });
        const title = wrapper.find('h1');
        expect(title.text()).toEqual('Test Title');
    });

    it(`it renders <h2> tag for 'subtitle' type`, () => {
        const wrapper = mount(Title, {
            propsData: {
                type: 'subtitle',
                text: 'Test Title'
            }
        });
        expect(wrapper.find('h2').exists()).toBeTruthy();
    });

    it('it sets correct text for subtitle', () => {
        const wrapper = mount(Title, {
            propsData: {
                type: 'subtitle',
                text: 'Test Title'
            }
        });
        const title = wrapper.find('h2');
        expect(title.text()).toEqual('Test Title');
    });

    it(`it renders <h3> tag for 'sectionTitle' type`, () => {
        const wrapper = mount(Title, {
            propsData: {
                type: 'sectionTitle',
                text: 'Test Title'
            }
        });
        expect(wrapper.find('h3').exists()).toBeTruthy();
    });

    it('it sets correct text for sectionTitle', () => {
        const wrapper = mount(Title, {
            propsData: {
                type: 'sectiontitle',
                text: 'Test Title'
            }
        });
        const title = wrapper.find('h3');
        expect(title.text()).toEqual('Test Title');
    });

    it(`it renders nothing for unknown types`, () => {
        const wrapper = mount(Title, {
            propsData: {
                type: 'unknown',
                text: 'Test Title'
            }
        });
        expect(wrapper.isEmpty()).toBeTruthy();
    });

    it(`it is case-insensitive on type`, () => {
        const wrapper = mount(Title, {
            propsData: {
                type: 'TiTLe',
                text: 'Test Title'
            }
        });
        expect(wrapper.find('h1').exists()).toBeTruthy();
    });

    it(`it adds 'formly-form-title' class to container`, () => {
        const wrapper = mount(Title, {
            propsData: {
                type: 'title',
                text: 'Test Title'
            }
        }, localVue);
        expect(wrapper.classes()).toContain('formly-form-title');
    });

    it(`it adds 'col-12' class to container if $formlyUiFramework is set to bootstrap`, () => {
        const localVue = createLocalVue();
        localVue.use(VueFormly, {
            uiFramework: 'bootstrap'
        });

        const wrapper = mount(Title, {
            propsData: {
                type: 'title',
                text: 'Test Title'
            }
        }, localVue);
        expect(wrapper.classes()).toContain('col-12');
    });
});
