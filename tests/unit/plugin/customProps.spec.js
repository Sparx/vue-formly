import * as CustomProps from '../../../src/plugin/customProps';

describe('CustomProps utils', () => {
    describe('LabelValidator method', () => {
        it('it returns true for an object with text and inputId properties', () => {
            const label = {
                text: 'Text',
                inputId: 'Id'
            };
            expect(CustomProps.LabelValidator(label)).toBeTruthy();
        });

        it('it returns false for an object missing text or inputId properties', () => {
            const label = {
                text: 'Text'
            };
            expect(CustomProps.LabelValidator(label)).toBeFalsy();
        });

        it('it returns false for any non-conforming value', () => {
            expect(CustomProps.LabelValidator(34)).toBeFalsy();
        });
    });
});
