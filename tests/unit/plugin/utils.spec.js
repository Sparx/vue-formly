import * as utils from '../../../src/plugin/utils';

describe('Utils', () => {
    describe('getInputClasses', () => {
        it('it returns formly-form-control if framework is undefined', () => {
            expect(utils.getInputClasses(undefined)).toEqual('formly-form-control');
        });

        it('it returns formly-form-control if framework is unknown', () => {
            expect(utils.getInputClasses('test')).toEqual('formly-form-control');
        });

        it('it includes form-contol in return value if framework is bootstrap', () => {
            const result = utils.getInputClasses('bootstrap');
            expect(result).toEqual(expect.stringContaining('form-control'));
        });

        it('framework check is case-insensitive', () => {
            const result = utils.getInputClasses('bootstrap');
            expect(result).toEqual(expect.stringContaining('form-control'));
        });
    });

    describe('getContainerClasses', () => {
        it('it returns formly-form-group if framework is undefined', () => {
            expect(utils.getContainerClasses(undefined)).toEqual('formly-form-group');
        });

        it('it returns formly-form-group if framework is unknown', () => {
            expect(utils.getContainerClasses('test')).toEqual('formly-form-group');
        });

        it('it includes form-group in return value if framework is bootstrap', () => {
            const result = utils.getContainerClasses('bootstrap');
            expect(result).toEqual(expect.stringContaining('form-group'));
        });

        it('framework check is case-insensitive', () => {
            const result = utils.getContainerClasses('bootSTRAP');
            expect(result).toEqual(expect.stringContaining('form-group'));
        });
    });

    describe('getSectionClasses', () => {
        it('it returns formly-form-section if framework is undefined', () => {
            expect(utils.getSectionClasses(undefined)).toEqual('formly-form-section');
        });

        it('it returns formly-form-section if framework is unknown', () => {
            expect(utils.getSectionClasses('test')).toEqual('formly-form-section');
        });

        it('it includes row in return value if framework is bootstrap', () => {
            const result = utils.getSectionClasses('bootstrap');
            expect(result).toEqual(expect.stringContaining('row'));
        });

        it('framework check is case-insensitive', () => {
            const result = utils.getSectionClasses('bootSTRAP');
            expect(result).toEqual(expect.stringContaining('row'));
        });
    });

    describe('getTitleClasses', () => {
        it('it returns formly-form-title if framework is undefined', () => {
            expect(utils.getTitleClasses(undefined)).toEqual('formly-form-title');
        });

        it('it returns formly-form-title if framework is unknown', () => {
            expect(utils.getTitleClasses('test')).toEqual('formly-form-title');
        });

        it('it includes col-12 in return value if framework is bootstrap', () => {
            const result = utils.getTitleClasses('bootstrap');
            expect(result).toEqual(expect.stringContaining('col-12'));
        });

        it('framework check is case-insensitive', () => {
            const result = utils.getTitleClasses('bootSTRAP');
            expect(result).toEqual(expect.stringContaining('col-12'));
        });
    });

    describe('getFreeTextClasses', () => {
        it('it returns formly-form-text if framework is undefined', () => {
            expect(utils.getFreeTextClasses(undefined)).toEqual('formly-form-text');
        });

        it('it returns formly-form-text if framework is unknown', () => {
            expect(utils.getFreeTextClasses('test')).toEqual('formly-form-text');
        });

        it('it includes col-12 in return value if framework is bootstrap', () => {
            const result = utils.getFreeTextClasses('bootstrap');
            expect(result).toEqual(expect.stringContaining('col-12'));
        });

        it('framework check is case-insensitive', () => {
            const result = utils.getFreeTextClasses('bootSTRAP');
            expect(result).toEqual(expect.stringContaining('col-12'));
        });
    });

    describe('getRadioCheckInputClasses', () => {
        it('it returns nothing if framework is undefined', () => {
            expect(utils.getRadioCheckInputClasses(undefined)).toEqual('');
        });

        it('it returns nothing if framework is unknown', () => {
            expect(utils.getRadioCheckInputClasses('test')).toEqual('');
        });

        it('it returns form-check if framework is bootstrap', () => {
            expect(utils.getRadioCheckInputClasses('bootstrap')).toEqual('form-check-input');
        });

        it('framework check is case-insensitive', () => {
            expect(utils.getRadioCheckInputClasses('bootSTRAP')).toEqual('form-check-input');
        });
    });

    describe('getRadioCheckLabelClasses', () => {
        it('it returns nothing if framework is undefined', () => {
            expect(utils.getRadioCheckLabelClasses(undefined)).toEqual('');
        });

        it('it returns nothing if framework is unknown', () => {
            expect(utils.getRadioCheckLabelClasses('test')).toEqual('');
        });

        it('it returns form-check if framework is bootstrap', () => {
            expect(utils.getRadioCheckLabelClasses('bootstrap')).toEqual('form-check-label');
        });

        it('framework check is case-insensitive', () => {
            expect(utils.getRadioCheckLabelClasses('bootSTRAP')).toEqual('form-check-label');
        });
    });

    describe('getRadioCheckContainerClasses', () => {
        it('it returns formly-form-radio if framework is undefined', () => {
            expect(utils.getRadioCheckContainerClasses(undefined)).toEqual('formly-form-radio');
        });

        it('it returns formly-form-radio if framework is unknown', () => {
            expect(utils.getRadioCheckContainerClasses('test')).toEqual('formly-form-radio');
        });

        it('it returns formly-form-check if isCheckboxContainer is true', () => {
            expect(utils.getRadioCheckContainerClasses('', true)).toEqual('formly-form-check');
        });

        it('it includes col-12 in return value if framework is bootstrap', () => {
            const result = utils.getRadioCheckContainerClasses('bootstrap');
            expect(result).toEqual(expect.stringContaining('form-check'));
        });

        it('framework check is case-insensitive', () => {
            const result = utils.getRadioCheckContainerClasses('bootSTRAP');
            expect(result).toEqual(expect.stringContaining('form-check'));
        });
    });

    describe('getSubmitButtonClasses', () => {
        it('it returns formly-submit if framework is undefined', () => {
            expect(utils.getSubmitButtonClasses(undefined)).toEqual('formly-submit');
        });

        it('it returns formly-submit if framework is unknown', () => {
            expect(utils.getSubmitButtonClasses('test')).toEqual('formly-submit');
        });

        it('it includes btn in return value if framework is bootstrap', () => {
            const result = utils.getSubmitButtonClasses('bootstrap');
            expect(result).toEqual(expect.stringContaining('btn'));
        });

        it('framework check is case-insensitive', () => {
            const result = utils.getSubmitButtonClasses('bootSTRAP');
            expect(result).toEqual(expect.stringContaining('btn'));
        });
    });

    describe('getValidationClasses', () => {
       it('it returns validation-error if framework is undefined', () => {
         expect(utils.getValidationClasses(undefined)).toEqual('validation-error');
       });

        it('it returns validation-error if framework is unknown', () => {
            expect(utils.getValidationClasses('test')).toEqual('validation-error');
        });

        it('it includes invalid-feedback if framework is bootstrap', () => {
            const result = utils.getValidationClasses('bootstrap');
            expect(result).toEqual(expect.stringContaining('invalid-feedback'));
        });

        it('framework check is case-insensitive', () => {
            const result = utils.getValidationClasses('BOOTstrap');
            expect(result).toEqual(expect.stringContaining('invalid-feedback'));
        });
    });
});
