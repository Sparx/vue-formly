import Vue from 'vue';
import VueFormly from '@/plugin/vue-formly';
import { createLocalVue } from '@vue/test-utils';

describe('VueFormly plugin install', () => {
    let localVue;
    beforeEach(() => {
        localVue = createLocalVue();
    });

    it('it sets the $formlyUiFramework property of Vue instance', () => {
        localVue.use(VueFormly, {
            uiFramework: 'testFramework'
        });
        expect(localVue.prototype.$formlyUiFramework).toEqual('testFramework');
    });

    it('it leaves the $formlyUiFramework property of Vue instance empty if no uiFramework property is present in opts', () => {
        localVue.use(VueFormly, { });
        expect(localVue.prototype.$formlyUiFramework).toEqual('');
    });

    it('it creates the $formlyEvents event hub', () => {
        localVue.use(VueFormly);
        expect(localVue.prototype.$formlyEvents).toBeInstanceOf(Vue);
    });
});
