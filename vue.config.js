module.exports = {
    configureWebpack: {
        devServer: {
            setup(app) {
                const bodyParser = require('body-parser');
                app.use(bodyParser.json());

                app.post('/submit', bodyParser.json(), function(req, res) {
                    /* eslint-disable-next-line */
                    console.log(req.body);
                    setTimeout(function() {                        
                        res.sendStatus(200);
                    }, 5000);
                });
            }
        }
    }
};